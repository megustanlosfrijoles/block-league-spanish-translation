local srcpath = minetest.get_modpath("bl_tutorial") .. "/src"

bl_tutorial = {}

dofile(srcpath .. "/_load.lua")
dofile(srcpath .. "/entities/ball.lua")
dofile(srcpath .. "/entities/obstacle.lua")
dofile(srcpath .. "/entities/sentry_gun.lua")
dofile(srcpath .. "/hud.lua")
dofile(srcpath .. "/nodes.lua")
dofile(srcpath .. "/phases.lua")