local S = minetest.get_translator("bl_tutorial")



-- function taken from Minetest Game
local function rotate_and_place(itemstack, placer, pointed_thing)
	local p0 = pointed_thing.under
	local p1 = pointed_thing.above
	local param2 = 0

	if placer then
		local placer_pos = placer:get_pos()
		if placer_pos then
			local diff = vector.subtract(p1, placer_pos)
			param2 = minetest.dir_to_facedir(diff)
			-- The player places a node on the side face of the node he is standing on
			if p0.y == p1.y and math.abs(diff.x) <= 0.5 and math.abs(diff.z) <= 0.5 and diff.y < 0 then
				-- reverse node direction
				param2 = (param2 + 2) % 4
			end
		end

		local finepos = minetest.pointed_thing_to_face_pos(placer, pointed_thing)
		local fpos = finepos.y % 1

		if p0.y - 1 == p1.y or (fpos > 0 and fpos < 0.5)
				or (fpos < -0.5 and fpos > -0.999999999) then
			param2 = param2 + 20
			if param2 == 21 then
				param2 = 23
			elseif param2 == 23 then
				param2 = 21
			end
		end
	end
	return minetest.item_place(itemstack, placer, pointed_thing, param2)
end



local function register_floor_edge(n)
  minetest.register_node("bl_tutorial:floor_edge_" .. n, {
    description = "[BL Tutorial] " .. S("Floor edge") .. " " .. n,
    tiles = {
      "bl_tutorial_floor_edge" .. n .. ".png",
      "bl_tutorial_floor2.png"
    },
    paramtype2 = "facedir",
    groups = {cracky = 3},
    sounds = default.node_sound_stone_defaults()
  })

  minetest.register_node("bl_tutorial:floor_edge_" .. n .. "_slab", {
    description = "[BL Tutorial] Floor edge " .. n .. " " .. S("(slab)"),
    drawtype = "nodebox",
    tiles = {
      "bl_tutorial_floor_edge" .. n .. ".png",
      "bl_tutorial_floor2.png"
    },
    node_box = {
      type = "fixed",
      fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
    },
    paramtype2 = "facedir",
    groups = {cracky = 2},
    sounds = default.node_sound_stone_defaults()
  })
end





-- pavimenti
minetest.register_node("bl_tutorial:floor1", {
  description = "[BL Tutorial] " .. S("Light grey floor"),
  tiles = { "bl_tutorial_floor1.png" },
  groups = {cracky = 2},
  sounds = default.node_sound_stone_defaults()
})



minetest.register_node("bl_tutorial:floor2", {
  description = "[BL Tutorial] " .. S("Blue floor"),
  tiles = { "bl_tutorial_floor2.png" },
  groups = {cracky = 2},
  sounds = default.node_sound_stone_defaults()
})



minetest.register_node("bl_tutorial:floor2_slab", {
  description = "[BL Tutorial] " .. S("Blue floor") .. " " .. S("(slab)"),
  tiles = { "bl_tutorial_floor2.png" },
  drawtype = "nodebox",
  node_box = {
    type = "fixed",
    fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
  },
  paramtype2 = "facedir",
  groups = {cracky = 2},
  sounds = default.node_sound_stone_defaults(),

  on_place = function(itemstack, placer, pointed_thing)
    rotate_and_place(itemstack, placer, pointed_thing)
  end,
})



-- bordo pavimenti
register_floor_edge(1)
register_floor_edge(2)
register_floor_edge(3)



-- muro
minetest.register_node("bl_tutorial:wall", {
  description = "[BL Tutorial] " .. S("Wall"),
  tiles = {{name="bl_tutorial_wall.png", align_style="world", scale=4}},
  drawtype = "nodebox",
  paramtype = "light",
  paramtype2 = "facedir",
  node_box = {
    type = "fixed",
    fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
  },
  groups = {cracky = 2},
  sounds = default.node_sound_stone_defaults(),

  on_place = function(itemstack, placer, pointed_thing)
    rotate_and_place(itemstack, placer, pointed_thing)
  end,
})



-- recinzioni
default.register_fence_rail("bl_tutorial:fence", {
  description = "[BL Tutorial] " .. S("Fence"),
  texture = "bl_tutorial_fence.png",
  inventory_image = "default_fence_rail_overlay.png^bl_tutorial_fence.png^" ..
              "default_fence_rail_overlay.png^[makealpha:255,126,126",
  wield_image = "default_fence_rail_overlay.png^bl_tutorial_fence.png^" ..
              "default_fence_rail_overlay.png^[makealpha:255,126,126",
  material = "default:wood",
  groups = {cracky = 2},
  sounds = default.node_sound_wood_defaults()
})



minetest.register_node("bl_tutorial:chainlink_fence", {
  description = "[BL Tutorial] " .. S("Chain-link fence"),
  drawtype = "signlike",
  tiles = {"bl_tutorial_chainlink_fence.png"},
  inventory_image = "bl_tutorial_chainlink_fence.png",
  wield_image = "bl_tutorial_chainlink_fence.png",
  paramtype = "light",
  paramtype2 = "wallmounted",
  sunlight_propagates = true,
  selection_box = {type = "wallmounted"},
  node_box = {type = "wallmounted"},
  groups = {cracky = 2},
  sounds = default.node_sound_metal_defaults()
})



-- piattaforme
minetest.register_node("bl_tutorial:step1", {
  description = "[BL Tutorial] " .. S("Platform"),
  tiles = {"bl_tutorial_step.png"},
  drawtype = "nodebox",
  paramtype = "light",
  paramtype2 = "facedir",
  node_box = {
    type = "connected",
    fixed = {-0.45, -0.45, -0.45, 0.45, -0.25, 0.45},
    connect_left = {{-0.5, -0.45, -0.45, 0.45, -0.25, 0.45},{-0.45, -0.45, -0.45, 0.45, -0.25, 0.45}},
    connect_right = {{-0.45, -0.45, -0.45, 0.5, -0.25, 0.45},{-0.45, -0.45, -0.45, 0.45, -0.25, 0.45}}
  },
  connects_to = {"bl_tutorial:step1"},
  groups = {cracky = 2},
  sounds = default.node_sound_stone_defaults(),

  on_place = function(itemstack, placer, pointed_thing)
    rotate_and_place(itemstack, placer, pointed_thing)
  end,
})



minetest.register_node("bl_tutorial:step2", {
  description = "[BL Tutorial] " .. S("Platform 1/2"),
  tiles = {"bl_tutorial_step.png"},
  drawtype = "nodebox",
  paramtype = "light",
  paramtype2 = "facedir",
  node_box = {
    type = "connected",
    fixed = {-0.45, 0.05, -0.45, 0.45, 0.25, 0.45},
    connect_left = {{-0.5, 0.05, -0.45, 0.45, 0.25, 0.45},{-0.45, 0.05, -0.45, 0.45, 0.25, 0.45}},
    connect_right = {{-0.45, 0.05, -0.45, 0.5, 0.25, 0.45},{-0.45, 0.05, -0.45, 0.45, 0.25, 0.45}}
  },
  connects_to = {"bl_tutorial:step2"},
  groups = {cracky = 2},
  sounds = default.node_sound_stone_defaults(),

  on_place = function(itemstack, placer, pointed_thing)
    rotate_and_place(itemstack, placer, pointed_thing)
  end,
})



-- meta
minetest.register_node("bl_tutorial:goal", {
  description = "[BL Tutorial] " .. S("Goal floor"),
  tiles = {"bl_tutorial_goal.png"},
  groups = {cracky = 2},
})



minetest.register_node("bl_tutorial:goal_wall", {
  description = "[BL Tutorial] " .. S("Goal pole"),
  drawtype = "nodebox",
  tiles = {"bl_tutorial_goal_pole.png"},
  sunlight_propagates = sunlight,
  light_source = light_source,
  paramtype = "light",
  paramtype2 = "facedir",
  groups = {cracky = 2},
  sounds = default.node_sound_stone_defaults(),
  node_box = {
    type = "fixed",
    fixed = {
      {-0.5, -0.5, -0.5, 0.5, 0.0, 0.5},
      {-0.5, 0.0, 0.0, 0.0, 0.5, 0.5},
    },
  },
  on_place = function(itemstack, placer, pointed_thing)
    if pointed_thing.type ~= "node" then
      return itemstack
    end

    return rotate_and_place(itemstack, placer, pointed_thing)
  end,
})



-- barriera
minetest.register_node("bl_tutorial:invisible_wall", {
  description = "[BL Tutorial] " .. S("Invisible wall"),
  inventory_image = "bl_tutorial_invwall.png",
  wield_image = "bl_tutorial_invwall.png",
  drawtype = "airlike",
  paramtype = "light",
  sunlight_propagates = true,
  pointable = false,
  drop = "",
  groups = {not_in_creative_inventory = 1},
})