# Block League

Main mod. For a general README see [here]((README.md))

### Dependencies
* [achievements_lib](https://content.minetest.net/packages/Zughy/achievements_lib/) by me
* [arena_lib](https://content.minetest.net/packages/Zughy/arena_lib/) by me
* (bundled by arena_lib) [Chat Command Builder](https://content.minetest.net/packages/rubenwardy/lib_chatcmdbuilder/) by rubenwardy
* [controls](https://content.minetest.net/packages/BuckarooBanzay/controls/) by Arcelmi/BuckarooBanzay
* [panel_lib](https://content.minetest.net/packages/Zughy/panel_lib/) by me
* [skills](https://content.minetest.net/packages/giov4/skills/) by Giov4
* (optional) [Visible Wielditem](https://content.minetest.net/packages/LMD/visible_wielditem/) by appguru

### Set up an arena
1. run `/arenas create block_league <arena_name>`
2. enter the editor via `/arenas edit <arena_name>`
3. have fun customising

### Utility commands
* `/bladmin testkit`: gives you the bouncer and the in-game physics, to easily test your maps. The last object in the hotbar restores your inventory and physics
